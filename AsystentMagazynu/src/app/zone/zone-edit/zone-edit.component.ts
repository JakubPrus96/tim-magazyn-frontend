import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {ZoneService} from "../../shared/zone/zone.service";
import {AppComponent} from "../../app.component";

@Component({
  selector: 'app-zone-edit',
  templateUrl: './zone-edit.component.html',
  styleUrls: ['./zone-edit.component.css']
})
export class ZoneEditComponent implements OnInit {

  zone: any = {};
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private zoneService: ZoneService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.zoneService.get(id).subscribe((zone: any) => {
          if (zone) {
            this.zone = zone;
          }
        })
      }
    })
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  gotoList() {
    this.router.navigate(['/zone-list']);
  }

  save(form: NgForm) {
    this.zoneService.save(form, AppComponent.warehouseId).subscribe(result => {
      this.zone = result;
      this.gotoList();
    }, error => console.error(error));
  }

}
