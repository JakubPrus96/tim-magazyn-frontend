import {Component, OnInit} from '@angular/core';
import {ZoneService} from "../../shared/zone/zone.service";

@Component({
  selector: 'app-zone-list',
  templateUrl: './zone-list.component.html',
  styleUrls: ['./zone-list.component.css']
})
export class ZoneListComponent implements OnInit {
  zones: Array<any>;

  constructor(private zoneService: ZoneService) {
  }

  ngOnInit() {
    this.zoneService.getAll().subscribe(data => {
      this.zones = data;
    })
  }

}
