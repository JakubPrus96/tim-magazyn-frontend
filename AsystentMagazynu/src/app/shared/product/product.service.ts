import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {DataFactory} from "../../dataFactory";

@Injectable()
export class ProductService {

  public PRODUCTS_API = '/product';

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    return this.http.get(DataFactory.API_URL + '/product/all');
  }

  get(id: string) {
    return this.http.get(DataFactory.API_URL + this.PRODUCTS_API + '/' + id);
  }

  save(product: any): Observable<any> {
    let result: Observable<Object>;
    if (product['href']) {
      result = this.http.put(DataFactory.API_URL + this.PRODUCTS_API + '/1', product);
    } else {
      result = this.http.put(DataFactory.API_URL + this.PRODUCTS_API + '/1', product);
    }
    return result;
  }
}
