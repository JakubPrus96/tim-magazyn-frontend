import {Component, OnInit} from '@angular/core';
import {WarehouseService} from "../../shared/warehouse/warehouse.service";

@Component({
  selector: 'app-warehouse-list',
  templateUrl: './warehouse-list.component.html',
  styleUrls: ['./warehouse-list.component.css']
})
export class WarehouseListComponent implements OnInit {

  warehouses: Array<any>;

  constructor(private warehouseService: WarehouseService) {
  }

  ngOnInit() {
    this.warehouseService.getAll().subscribe(result => {
      this.warehouses = result;
    })
  }

}
