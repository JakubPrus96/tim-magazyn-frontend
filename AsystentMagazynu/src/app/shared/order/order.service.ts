import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {DataFactory} from "../../dataFactory";

@Injectable()
export class OrderService {


  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    return this.http.get(DataFactory.API_URL + '/order/all');
  }

  getOrderProducts(id: string): Observable<any> {
    return this.http.get(DataFactory.API_URL + '/order-product/all/' + id);
  }

  get(id: string) {
    return this.http.get(DataFactory.API_URL + '/order/' + id);
  }

  getRoute(id: string): Observable<any> {
    return this.http.get(DataFactory.API_URL + '/order/route/' + id);
  }

  save(order: any): Observable<any> {
    let result: Observable<Object>;
    if (order['href']) {
      result = this.http.put(order.href, order);
    } else {
      result = this.http.post(DataFactory.API_URL + '/order/1/', order);
    }
    return result;
  }
}
