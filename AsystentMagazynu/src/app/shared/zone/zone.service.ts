import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {DataFactory} from "../../dataFactory";

@Injectable()
export class ZoneService {

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    return this.http.get(DataFactory.API_URL + '/zone/all');
  }

  get(id: string) {
    return this.http.get(DataFactory.API_URL + '/zone/' + id);
  }

  save(zone: any, warehouseId: any): Observable<any> {
    let result: Observable<Object>;
    if (zone['href']) {
      result = this.http.put(zone.href, zone);
    } else {
      result = this.http.post(DataFactory.API_URL + '/zone/' + warehouseId + '/', zone);
    }
    return result;
  }

}
