import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {WarehouseService} from "../../shared/warehouse/warehouse.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-warehouse-edit',
  templateUrl: './warehouse-edit.component.html',
  styleUrls: ['./warehouse-edit.component.css']
})
export class WarehouseEditComponent implements OnInit {

  warehouse: any = {};
  sub: Subscription;

  constructor(private route: ActivatedRoute, private router: Router, private warehouseService: WarehouseService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.warehouseService.get(id).subscribe((warehouse: any) => {
          if (warehouse) {
            this.warehouse = warehouse;
          }
        })
      }
    })
  }

  gotoList() {
    this.router.navigate(['/warehouse-list']);
  }

  save(form: NgForm) {
    this.warehouseService.save(form).subscribe(result => {
      this.warehouse = result;
      this.gotoList();
    }, error => console.error(error));
  }
}
