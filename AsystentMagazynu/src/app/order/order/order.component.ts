import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {OrderService} from "../../shared/order/order.service";
import {Surface} from "@progress/kendo-drawing";
import {drawScene} from "../../draw/draw";
import {PlaceService} from "../../shared/place/place.service";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})

export class OrderComponent implements OnDestroy, OnInit {

  order: any = {};
  orderProducts: Array<any>;
  sub: Subscription;
  placesToCollect: Array<any>;
  allPlacesInZone: Array<any>;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private orderService: OrderService,
              private placeService: PlaceService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.setOrderProductsAndRouteMap(id);
      }
    })
  }

  private setOrderProductsAndRouteMap(id) {
    this.orderService.getOrderProducts(id).subscribe((orderProducts) => {
      this.orderProducts = orderProducts;
    });
    this.orderService.getRoute(id).subscribe((routeMap) => {
      this.placesToCollect = Object.values(routeMap);
      if (this.placesToCollect['0']) {
        this.placeService.getPlacesByZoneId(this.placesToCollect['0']['place']['zone']['id']).subscribe((allPlaces) => {
          this.allPlacesInZone = allPlaces;
          this.drawMap();
        });
      }
    })
  }

  @ViewChild('surface')
  private surfaceElement: ElementRef;
  private surface: Surface;

  drawMap(): void {
    drawScene(this.createSurface(), this.placesToCollect, this.allPlacesInZone);
  }

  public ngOnDestroy() {
    this.surface.destroy();
  }

  private createSurface(): Surface {
    const element = this.surfaceElement.nativeElement;
    this.surface = Surface.create(element, {width: '1050', height: '1050'});
    return this.surface;
  }
}
