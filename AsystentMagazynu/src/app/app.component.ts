import {Component, OnInit} from '@angular/core';
import {WarehouseService} from "./shared/warehouse/warehouse.service";

export interface Warehouse {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Asystent Magazynu';
  static warehouseId: number;
  warehouses: Array<any>;
  warehousePicklistValues: Array<Warehouse> = [];

  constructor(private warehouseService: WarehouseService) {
  }

  ngOnInit() {
    this.warehouseService.getAll().subscribe(result => {
      this.warehouses = result;
      this.setWarehouseInterface();
    })
  }

  private setWarehouseInterface() {
    this.warehouses.forEach((warehouse) => {
      this.warehousePicklistValues.push({
        value: warehouse.id, viewValue: warehouse.name
      });
    })
  }

  selectionChange(value: any) {
    AppComponent.warehouseId = value.value;
  }

}
