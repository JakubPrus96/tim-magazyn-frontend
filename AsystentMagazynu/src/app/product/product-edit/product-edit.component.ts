import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {ProductService} from "../../shared/product/product.service";
import {NgForm} from "@angular/forms";
import {AppComponent} from "../../app.component";

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

  product: any = {};
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private productService: ProductService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.productService.get(id).subscribe((product: any) => {
          if (product) {
            this.product = product;
          }
        })
      }
    })
  }

  gotoList() {
    this.router.navigate(['/product-list']);
  }

  save(form: NgForm) {
    this.productService.save(form).subscribe(result => {
      console.log(AppComponent.warehouseId);
      this.product = result;
      this.gotoList();
    }, error => console.error(error));
  }
}
