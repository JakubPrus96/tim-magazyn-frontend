import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {DataFactory} from "../../dataFactory";

@Injectable()
export class PlaceService {

  constructor(private http: HttpClient) {
  }

  getPlacesByZoneId(id: string): Observable<any> {
    return this.http.get(DataFactory.API_URL + '/place/all/' + id);
  }
}
