import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {ProductService} from "./shared/product/product.service";
import {ProductListComponent} from './product/product-list/product-list.component';
import {
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatListModule, MatSelectModule, MatSidenavModule, MatToolbarModule
} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ProductEditComponent} from './product/product-edit/product-edit.component';
import {RouterModule, Routes} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {HomePageComponent} from './home-page/home-page.component';
import {WarehouseListComponent} from './warehouse/warehouse-list/warehouse-list.component';
import {WarehouseEditComponent} from './warehouse/warehouse-edit/warehouse-edit.component';
import {ZoneEditComponent} from './zone/zone-edit/zone-edit.component';
import {ZoneListComponent} from './zone/zone-list/zone-list.component';
import {OrderListComponent} from './order/order-list/order-list.component';
import {OrderComponent} from './order/order/order.component';
import {WarehouseService} from "./shared/warehouse/warehouse.service";
import {ZoneService} from "./shared/zone/zone.service";
import {OrderService} from "./shared/order/order.service";
import {PlaceService} from "./shared/place/place.service";

const appRoutes: Routes = [
  {
    path: '',
    component: HomePageComponent
  }, {
    path: 'product-list',
    component: ProductListComponent
  }, {
    path: 'product-add',
    component: ProductEditComponent
  }, {
    path: 'product-edit/:id',
    component: ProductEditComponent
  }, {
    path: 'warehouse-list',
    component: WarehouseListComponent
  }, {
    path: 'warehouse-add',
    component: WarehouseEditComponent
  }, {
    path: 'warehouse-edit/:id',
    component: WarehouseEditComponent
  }, {
    path: 'order-list',
    component: OrderListComponent
  }, {
    path: 'order/:id',
    component: OrderComponent
  }, {
    path: 'zone-list',
    component: ZoneListComponent
  }, {
    path: 'zone-add',
    component: ZoneEditComponent
  }, {
    path: 'zone-edit/:id',
    component: ZoneEditComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    ProductEditComponent,
    HomePageComponent,
    WarehouseListComponent,
    WarehouseEditComponent,
    ZoneEditComponent,
    ZoneListComponent,
    OrderListComponent,
    OrderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    MatSelectModule,
    MatSidenavModule
  ],
  providers: [
    ProductService,
    WarehouseService,
    ZoneService,
    OrderService,
    PlaceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
