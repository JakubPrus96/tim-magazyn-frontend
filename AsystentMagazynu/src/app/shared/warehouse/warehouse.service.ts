import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {DataFactory} from "../../dataFactory";

@Injectable()
export class WarehouseService {

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    return this.http.get(DataFactory.API_URL + '/warehouse/all');
  }

  get(id: string) {
    return this.http.get(DataFactory.API_URL + '/warehouse/' + id);
  }

  save(warehouse: any): Observable<any> {
    let result: Observable<Object>;
    if (warehouse['href']) {
      result = this.http.post(warehouse.href, warehouse);
    } else {
      result = this.http.post(DataFactory.API_URL + '/warehouse', warehouse);
    }
    return result;
  }
}
