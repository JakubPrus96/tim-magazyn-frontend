import {Surface, Group, Rect, geometry, Path, Text} from '@progress/kendo-drawing';

const {Rect: RectGeometry, Point, Size, transform} = geometry;

export function drawScene(surface: Surface, placesToCollect: Array<any>, allPlacesInZone: Array<any>) {
  const group = new Group();

  drawZone(group, placesToCollect[0]['place']['zone']['width'], placesToCollect[0]['place']['zone']['length']);

  drawAllPlaces(allPlacesInZone, group);

  drawPlacesToCollect(placesToCollect, group);

  drawAllRoutes(placesToCollect, group);

  group.transform(
    transform().translate(50, 50)
  );
  surface.draw(group);
}

function drawZone(group, width, length) {
  const zone = new RectGeometry(
    new Point(0, 0),
    new Size(length, width)
  );

  const zoneRect = new Rect(zone, {
    stroke: {
      color: "#000000",
      width: 1
    }
  });
  group.append(zoneRect);
}

function drawAllPlaces(allPlacesInZone: Array<any>, group) {
  allPlacesInZone.forEach((place) => {
    let color;
    if (place['product'] === null) {
      color = '#FFFF00'
    } else {
      color = '#FF0000'
    }
    drawPlace(place, group, color, place['length'], place['width']);
  });
}

function drawPlacesToCollect(placesToCollect: Array<any>, group) {
  placesToCollect.forEach((place) => {
    drawPlace(place, group, '#126500', place['place']['length'], place['place']['width']);
    drawText(place, group);
  });
}

function drawPlace(place, group, color, length, width) {
  let rectGeometry = new RectGeometry(
    new Point(place['coordX'], place['coordY']),
    new Size(length, width)
  );
  let rect = new Rect(rectGeometry, {
    stroke: {
      color: "#480000",
      width: 1
    }, fill: {
      color: color
    }
  });
  group.append(rect);
}

function drawText(place, group) {
  const text = new Text(
    place['productName'],
    new Point(place['coordX'] + 5, place['coordY']),
    {
      font: '16px Arial; writing-mode: vertical-lr;',
    });
  group.append(text);
}

function drawAllRoutes(placesToCollect, group) {
  placesToCollect.forEach((place) => {
    drawRoute(place, group);
  });
}

function drawRoute(place, group) {
  const path = new Path({
    stroke: {
      color: "#9999b6",
      width: 4
    }
  });
  path.moveTo(place['previousCoordX'] + 5, place['previousCoordY'] + 5)
    .lineTo(place['coordX'] + 5, place['coordY'] + 5)
    .close();
  group.append(path);
}
